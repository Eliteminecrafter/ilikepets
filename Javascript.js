function myFunction() {
    let petInput     = "";
let msgCorrect   = "Your guess was correct, My favourite pet is ";
let msgIncorrect = "Your guess was incorrect, My favourite pet isn't ";
let msgError     = "!!!!!!!!Unrecognised pet!!!!!!!!";

let msgInstructions = "My favourite type of pet is - dog, cat, rabbit, fish and koala \n";
                      "Guess which pet is my favourite. ";

petInput = prompt(msgInstructions); //Prompts the message instructions

//determining whether the user has entered the right answer or not.
petInput = petInput.toLowerCase(); //means it doesn't matter what case the user types in e.g E or e means the same thing
switch(petInput) {
    case "dog":
        alert(`${msgIncorrect} ${petInput}`)
    case "cat":
        alert(`${msgIncorrect} ${petInput}`)  
    case "rabbit":
        alert(`${msgIncorrect} ${petInput}`)   
    case "fish":
        alert(`${msgCorrect} ${petInput}`)
    case "koala":
        alert(`${msgIncorrect} ${petInput}`) 
    default:
        alert(`${msgError} ${petInput}`)              
} 

}
